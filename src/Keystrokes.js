
class Keystrokes {
  #NO_KEYPRESS = ""
  #LETTER_SEPARATOR = "1"
  #WHITE_SPACE = "0"

  #keystrokes = []

  static from(raw) {
    return new Keystrokes(raw)
  }

  constructor(raw) {
    this.#keystrokes = raw.split(this.#WHITE_SPACE)
  }

  map(callback) {
    return this.#keystrokes.map((dirtyKeystrokes) => {
      const keystrokes = dirtyKeystrokes.split(this.#LETTER_SEPARATOR)

      const letters = keystrokes.map((rawKeypress) => {
        if (!this.#isKeyPress(rawKeypress)) { return this.#NO_KEYPRESS }

          return callback(rawKeypress)
      })

      return letters.join(this.#NO_KEYPRESS)
    })
  }

  #isKeyPress(value) {
    return (value !== '')
  }
}

module.exports = { Keystrokes }
