const { Keystrokes } = require("./Keystrokes")
const { Keypress } = require("./Keypress")

class Mockia {
  static transform(rawKeystrokes) {
    const keystrokes = Keystrokes.from(rawKeystrokes)
    const text = keystrokes.map((rawKeypress) => {
      const keypress = Keypress.from(rawKeypress)

      return keypress.toLetter()
    })

    return text.join(" ")
  }
}

module.exports = { Mockia }
