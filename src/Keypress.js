
class Keypress {
  #UPPER_CASE_KEY = "#"
  #TRANSFORM_TO_LETTER = {
    "2": "a",
    "22": "b"
    //...
  }
  #raw = null

  static from(raw) {
    return new Keypress(raw)
  }

  constructor(raw) {
    this.#raw = raw
  }

  toLetter() {
    const letter = this.#transform_keypress_to_letter()

    if (this.#HasToBeInUpperCase()) {
      return letter.toUpperCase()
    } else {
      return letter.toLowerCase()
    }
  }

  #HasToBeInUpperCase() {
    return this.#raw.includes(this.#UPPER_CASE_KEY)
  }

  #transform_keypress_to_letter() {
    return this.#TRANSFORM_TO_LETTER[this.#keypress()]
  }

  #keypress() {
    if (this.#HasToBeInUpperCase()) {
      let keypress = this.#raw.substring(1)

      return keypress
    } else {
      return this.#raw
    }
  }
}

module.exports = { Keypress }
