FROM node:19-slim
WORKDIR /opt/kata

COPY package* .

RUN npm install

COPY * .
