const { Mockia } = require("../src/Mockia")
const { expect } = require("chai")

describe("Mockia", () => {
  it("transforms keypress Zero as a blank space", () => {

    const text = Mockia.transform("0")

    expect(text).to.eq(" ")
  })

  it("transforms keystrokes Zero as a blank spaces", () => {

    const text = Mockia.transform("00")

    expect(text).to.eq("  ")
  })

  it("transforms keypress Two as a 'a'", () => {

    const text = Mockia.transform("2")

    expect(text).to.eq("a")
  })

  it("transforms keystrokes Two as a 'b'", () => {

    const text = Mockia.transform("22")

    expect(text).to.eq("b")
  })

  it("transforms keypress One is used to separete letters with the same number", () => {

    const text = Mockia.transform("2212")

    expect(text).to.eq("ba")
  })

  it("transforms keypress hash next letter in uppercase indicator", () => {

    const text = Mockia.transform("#22")

    expect(text).to.eq("B")
  })
})
