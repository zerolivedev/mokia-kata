# Mokia kata from Mauri

## System requirements

- Docker version 20.10.18 or compatible
- Docker Compose version v2.3.3 or compatible

## How to run test

- Build the project with `docker compose up --build` and after this use:

`docker compose run --rm kata npm test`

> Once you done to work in this project remember to stop it with `docke compose down`

## Problem to solve

Back to 2001: Mockia 3310
Inspired by mario.mg’s Codewars kata (Phonewords)






The idea of this kata is to create a program which allows us to send SMS.
In order to do that, a string of numbers will be passed to our program and this must be transformed into text.

The numbers in the string represent the key we want to click in our Mockia to get the desired letter.
Therefore:
“3” = “d”
“33” = “e”
“33666”= “eo”

Basic Level:
Convert one word strings:

"55282"-→"kata"

Intermediate Level:
The key 0 represents a space in the string.
String is always lowercase.
On an empty string, return empty string.

"443355555566604466690277733099966688"  -->  "hello how are you"
"000" -->  "   "
Intermedite-superior Level:
Keyword 1 is used to separete letters with the same number.
"7717777" --> "qs"

Superior Level:
If consecutive numbers don’t have a “1” between them, always transform the number to the letter with the maximum value.
This way: "777777" --> "sq" in contrast to the previous level where  "7717777" --> "qs"


Further development?
Hash symbol to put Uppercase?
\# symbol to…
